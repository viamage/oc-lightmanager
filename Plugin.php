<?php namespace Viamage\LightManager;

use System\Classes\PluginBase;
use Viamage\LightManager\Components\AddTask;
use Viamage\LightManager\Components\EditTask;
use Viamage\LightManager\Components\Sleep;
use Viamage\LightManager\Components\TaskList;
use Viamage\LightManager\Components\Manager;
use Viamage\LightManager\Console\DetectDevices;
use Viamage\LightManager\Console\RunTasks;

/**
 * LightManager Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails(): array
    {
        return [
            'name'        => 'LightManager',
            'description' => 'No description provided yet...',
            'author'      => 'Viamage',
            'icon'        => 'icon-leaf',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register(): void
    {
        $this->commands(DetectDevices::class, RunTasks::class);
    }

    public function registerSchedule($schedule): void
    {
        $schedule->command('wemo:run-tasks')->everyMinute();
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return void
     */
    public function boot(): void
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents(): array
    {
        return [
            Manager::class  => 'device_manager',
            TaskList::class => 'task_list',
            AddTask::class  => 'add_task',
            EditTask::class => 'edit_task',
            Sleep::class    => 'sleep',
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation(): array
    {
        return []; // Remove this line to activate
    }
}
