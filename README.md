# oc-lightmanager

Drop-in replacement for Wemo mobile app.

Fully supports switches and lightbulb, more to come, maybe, PRs are welcome.

## Installation

*todo: More detailed instructions on deploying on RPi here*

- Install OctoberCMS
- Install plugin at plugins/viamage/lightmanager
- Install [theme](https://bitbucket.org/viamage/oc-lightmanager-theme/src/master/) in themes
- Install Keios Apparatus with php artisan plugin:install keios.apparatus
- Install RainLab.User with php artisan plugin:install rainlab.user
- Install Supervisor and setup services for

```
[program:light]
process_name=%(program_name)s_%(process_num)02d
directory = /path/to/october # change path!
command = php /path/to/october/artisan queue:work --tries=3 --daemon # change path!
autostart = true
numprocs = 4
user = http # change it!
autorestart = true
stdout_events_enabled=true
stderr_events_enabled=true
redirect_stderr=true
stdout_logfile=/var/log/supervisor/queue.log 
```

Depending on your devices amount you may increase numprocs, so the dimming / sleeping tasks wouldn't overlap and stay in pending state.

- Setup Cron with

```
* * * * * php /path/to/october/artisan schedule:run
```

- Create your user, setup active theme in backend
- Connect your Wemo devices using vendor app and run:

```
php artisan wemo:detect
```

in october's root.

### Setting up webserver

*todo - tricky as mobile app will require url. for now you need to figure it yourself*

### Styling

Theme uses [SemanticUI](https://semantic-ui.com/). It should be quite easy to style, just read the docs!

### Generating Mobile App

*This can be quite complex, manual and repo on this is todo*
