<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 5/26/18
 * Time: 5:37 PM
 */

namespace Viamage\LightManager\Classes;

use a15lam\PhpWemo\Discovery;
use October\Rain\Exception\ApplicationException;
use Viamage\LightManager\Repositories\DeviceRepository;

/**
 * Class CommandSender
 * @package Viamage\LightManager\Classes
 */
class CommandSender
{
    /**
     * @var DeviceRepository
     *
     */
    public $deviceRepo;

    /**
     * CommandSender constructor.
     * @param DeviceRepository|null $repo
     */
    public function __construct(DeviceRepository $repo = null)
    {
        if ($repo) {
            $this->deviceRepo = $repo;
        } else {
            $this->deviceRepo = \App::make(DeviceRepository::class);
        }
    }

    /**
     * @param string $slug
     * @throws \Exception
     */
    public function toggle(string $slug): void
    {
        $deviceDb = $this->deviceRepo->getBySlug($slug);
        if(!$deviceDb){
            throw new ApplicationException('Device not found!');
        }
        $device = Discovery::getDeviceByName($deviceDb->name);
        if ($device->state()) {
            $device->Off();
        } else {
            $device->On();
        }
    }

    /**
     * @param string $slug
     * @throws \Exception
     */
    public function switchOn(string $slug): void
    {
        $deviceDb = $this->deviceRepo->getBySlug($slug);
        if(!$deviceDb){
            throw new ApplicationException('Device not found!');
        }
        $device = Discovery::getDeviceByName($deviceDb->name);
        $device->On();
    }

    /**
     * @param string $slug
     * @throws \Exception
     */
    public function switchOff(string $slug): void
    {
        $deviceDb = $this->deviceRepo->getBySlug($slug);
        if(!$deviceDb){
            throw new ApplicationException('Device not found!');
        }
        $device = Discovery::getDeviceByName($deviceDb->name);
        $device->Off();
    }

    /**
     * @param string $slug
     * @param int    $dim
     * @throws \Exception
     */
    public function setDim(string $slug, int $dim): void
    {
        $deviceDb = $this->deviceRepo->getBySlug($slug);
        if(!$deviceDb){
            throw new ApplicationException('Device not found!');
        }
        $device = Discovery::getDeviceByName($deviceDb->name);
        $device->dim($dim);
    }
}