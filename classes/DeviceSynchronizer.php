<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 5/26/18
 * Time: 10:58 AM
 */

namespace Viamage\LightManager\Classes;

use a15lam\PhpWemo\Discovery;
use Viamage\LightManager\Models\Device;
use Viamage\LightManager\Repositories\DeviceRepository;

/**
 * Class DeviceSynchronizer
 * @package Viamage\LightManager\Classes
 */
class DeviceSynchronizer
{
    /**
     * @var DeviceRepository
     */
    private $repo;

    /**
     * DeviceSynchronizer constructor.
     */
    public function __construct()
    {
        $this->repo = new DeviceRepository();
    }

    /**
     * @return array
     */
    public function scanForDevices(): array
    {
        $devices = Discovery::find(true);
        $result = [];
        foreach ($devices as $device) {
            if (array_key_exists('device', $device)) {
                foreach ($device['device'] as $subDevice) {
                    $result[] = $this->createDevice($subDevice);
                }
            } else {

                $result[] = $this->createDevice($device);
            }
        }

        return $result;
    }

    /**
     * @param array $deviceData
     * @param array $metadata
     * @return Device
     */
    private function createDevice(array $deviceData, array $metadata = []): Device
    {
        $ip = '';
        $type = 'unknown';
        $name = 'unknown';
        if (array_key_exists('FriendlyName', $deviceData)) {
            $name = $deviceData['FriendlyName'];
        }
        if (array_key_exists('friendlyName', $deviceData)) {
            $name = $deviceData['friendlyName'];
        }
        if (array_key_exists('productName', $deviceData)) {
            $type = $deviceData['productName'];
        }
        if (array_key_exists('modelName', $deviceData)) {
            $type = $deviceData['modelName'];
        }
        if (array_key_exists('ip', $deviceData)) {
            $ip = $deviceData['ip'];
        }
        $data = [
            'name'      => $name,
            'slug'      => strtolower(str_replace([' ', '.', ','], '', $name)),
            'type'      => $type,
            'ip'        => $ip,
            'metadata'  => $metadata,
            'is_online' => false,
        ];

        if ($device = $this->repo->getBySlug($data['slug'])) {
            $device->fill($data);
            $device->save();
        } else {
            $device = new Device();
            $device->fill($data);
            $device->save();
        }

        return $device;
    }
}