<?php namespace Viamage\LightManager\Components;

use a15lam\PhpWemo\Discovery;
use Cms\Classes\ComponentBase;
use Illuminate\Http\RedirectResponse;
use October\Rain\Exception\ApplicationException;
use Viamage\LightManager\Models\Task;
use Viamage\LightManager\Repositories\DeviceRepository;

/**
 * Class AddTask
 * @package Viamage\LightManager\Components
 */
class AddTask extends ComponentBase
{
    /**
     * @var DeviceRepository
     */
    private $repo;

    /**
     * AddTask constructor.
     * @param null  $cmsObject
     * @param array $properties
     */
    public function __construct($cmsObject = null, array $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $this->repo = \App::make(DeviceRepository::class);
    }

    /**
     * @return array
     */
    public function componentDetails(): array
    {
        return [
            'name'        => 'AddTask Component',
            'description' => 'No description provided yet...',
        ];
    }

    /**
     *
     */
    public function onRun(): void
    {
        $this->page['devices'] = $this->repo->getAll();
    }

    /**
     * Ajax method
     *
     * @return void
     * @throws \Exception
     */
    public function onSelectDevice(): void
    {
        $deviceDb = $this->repo->getBySlug(post('device'));
        if(!$deviceDb){
            throw new ApplicationException('Device not found!');
        }
        $device = Discovery::getDeviceByName($deviceDb->name);
        $actions = [
            'on'  => 'Switch on',
            'off' => 'Switch off',
        ];
        if ($device->isDimmable()) {
            $actions['dim'] = 'Dim';
        }
        $this->page['actions'] = $actions;
    }

    /**
     * Ajax method
     *
     * @return void
     * @throws \Exception
     */
    public function onSelectAction(): void
    {
        $deviceDb = $this->repo->getBySlug(post('device'));
        if(!$deviceDb){
            throw new ApplicationException('Device not found!');
        }
        $device = Discovery::getDeviceByName($deviceDb->name);
        $this->page['action'] = post('action');
        if ($device->isDimmable()) {
            $this->page['dimmable'] = true;
        }
    }

    /**
     * @return RedirectResponse
     * @throws \October\Rain\Exception\ApplicationException
     */
    public function onAddTask()
    {
        $data = post();
        $this->validateAddTask();
        $task = new Task();
        $task->name = $data['name'];
        $task->action = $data['action'];
        $task->action_time = str_replace(' : ', ':', $data['action_time']);
        $task->setActionDaysFromPost($data['weekdays']);
        $task->is_active = array_key_exists('is_active', $data) && $data['is_active'] === 'on';

        if (array_key_exists('dim', $data)) {
            $task->dim = $data['dim'];
        }
        if (array_key_exists('dim_time', $data)) {
            $task->dim_time = $data['dim_time'];
        }
        $device = $this->repo->getBySlug($data['device']);
        if(!$device){
            throw new ApplicationException('Device not found');
        }
        $task->device_id = $device->id;
        $task->save();
        \Flash::success('Task added successfully');
        Task::cacheClear($task->id);

        return \Redirect::to('/tasks');
    }

    /**
     *
     */
    private function validateAddTask(): void
    {
        $data = post();
        $required = [
            'name'        => 'required',
            'action'      => 'required',
            'action_time' => 'required',
            'weekdays'    => 'required',
            'device'      => 'required',
        ];
        $v = \Validator::make($data, $required);
        if ($v->fails()) {
            throw new \ValidationException($v);
        }
    }
}
