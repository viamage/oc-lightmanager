<?php namespace Viamage\LightManager\Components;

use a15lam\PhpWemo\Discovery;
use Cms\Classes\ComponentBase;
use October\Rain\Exception\ApplicationException;
use Viamage\LightManager\Models\Task;
use Viamage\LightManager\Repositories\DeviceRepository;
use Viamage\LightManager\Repositories\TaskRepository;

/**
 * Class EditTask
 * @package Viamage\LightManager\Components
 */
class EditTask extends ComponentBase
{
    /**
     * @return array
     */
    public function componentDetails(): array
    {
        return [
            'name'        => 'EditTask Component',
            'description' => 'No description provided yet...',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties(): array
    {
        return [
            'id' => [
                'label'       => 'ID',
                'description' => 'Task ID',
                'default'     => '{{ :id }}',
                'type'        => 'string',
            ],
        ];
    }

    /**
     * @var TaskRepository
     */
    private $taskRepository;

    /**
     * @var DeviceRepository
     */
    private $deviceRepository;

    /**
     * EditTask constructor.
     * @param null  $cmsObject
     * @param array $properties
     */
    public function __construct($cmsObject = null, array $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $this->taskRepository = \App::make(TaskRepository::class);
        $this->deviceRepository = \App::make(DeviceRepository::class);
    }

    /**
     *
     * @throws \Exception
     */
    public function onRun(): void
    {
        /** @var Task $task */
        $this->page['task'] = $task = $this->taskRepository->getById($this->property('id'));
        $days = $task->actionDaysToArray();
        $weekdays = [];
        foreach ($days as $key => $value) {
            $weekdays[$value] = 1;
        }
        $this->page['weekdays'] = $weekdays;
        $this->page['devices'] = $this->deviceRepository->getAll();
        $deviceDb = $task->device;
        $this->page['device'] = $device = Discovery::getDeviceByName($deviceDb->name);
        $actions = [
            'on'  => 'Switch on',
            'off' => 'Switch off',
        ];
        if ($device->isDimmable()) {
            $actions['dim'] = 'Dim';
        }

        $this->page['actions'] = $actions;
    }

    /**
     * @return mixed
     * @throws \October\Rain\Exception\ApplicationException
     */
    public function onUpdateTask()
    {
        $data = post();
        $this->validateEditTask();
        $task = $this->taskRepository->getById($this->property('id'));
        if(!$task){
            throw new ApplicationException('Task has been removed!');
        }
        $task->name = $data['name'];
        $task->action = $data['action'];
        $task->action_time = str_replace(' : ', ':', $data['action_time']);
        $task->setActionDaysFromPost($data['weekdays']);
        $task->is_active = array_key_exists('is_active', $data) && $data['is_active'] === 'on';
        if (array_key_exists('dim', $data)) {
            $task->dim = $data['dim'];
        }
        if (array_key_exists('dim_time', $data)) {
            $task->dim_time = $data['dim_time'];
        }

        $device = $this->deviceRepository->getBySlug($data['device']);
        if(!$device){
            throw new ApplicationException('Device not found!');
        }
        $task->device_id = $device->id;
        $task->save();

        \Flash::success('Task updated successfully');
        Task::cacheClear($task->id);

        return \Redirect::to('/task/'.$this->property('id'));
    }

    /**
     * Ajax method
     *
     * @return void
     * @throws \Exception
     */
    public function onSelectAction(): void
    {
        $deviceDb = $this->deviceRepository->getBySlug(post('device'));
        if(!$deviceDb){
            throw new ApplicationException('Device not found!');
        }
        $device = Discovery::getDeviceByName($deviceDb->name);
        $this->page['action'] = post('action');
        if ($device->isDimmable()) {
            $this->page['dimmable'] = true;
        }
    }

    /**
     * @throws \ValidationException
     */
    private function validateEditTask(): void
    {
        $data = post();
        $required = [
            'name'        => 'required',
            'action'      => 'required',
            'action_time' => 'required',
            'weekdays'    => 'required',
            'device'      => 'required',
        ];
        $v = \Validator::make($data, $required);
        if ($v->fails()) {
            throw new \ValidationException($v);
        }
    }
}
