<?php namespace Viamage\LightManager\Components;

use a15lam\PhpWemo\Discovery;
use Cms\Classes\ComponentBase;
use Session;
use Viamage\LightManager\Classes\CommandSender;
use Viamage\LightManager\Repositories\DeviceRepository;

/**
 * Class Manager
 * @package Viamage\LightManager\Components
 */
class Manager extends ComponentBase
{

    /**
     * @var DeviceRepository
     */
    private $repo;

    /**
     * @var CommandSender
     */
    private $commandSender;

    /**
     * Manager constructor.
     * @param null  $cmsObject
     * @param array $properties
     */
    public function __construct($cmsObject = null, array $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $this->repo = \App::make(DeviceRepository::class);
        $this->commandSender = new CommandSender($this->repo);
    }

    /**
     * @return array
     */
    public function componentDetails(): array
    {
        return [
            'name'        => 'Manager Component',
            'description' => 'No description provided yet...',
        ];
    }

    /**
     *
     * @throws \Exception
     */
    public function onRun(): void
    {
        $sessionHidden = [];
        if (Session::has('session_hidden')) {
            $sessionHidden = Session::get('session_hidden');
        }
        $deviceList = $this->repo->getAll();
        $names = [];
        $devices = [];
        foreach ($deviceList as $deviceDb) {
            $devices[$deviceDb->slug] = Discovery::getDeviceByName($deviceDb->name);
            $names[$deviceDb->slug] = $deviceDb->name;
        }

        $this->page['local_devices'] = $deviceList;
        $this->page['devices'] = $devices;
        $this->page['device_names'] = $names;
        $this->page['device_icons'] = $this->getDeviceIcons($devices);
        $this->page['session_hidden'] = $sessionHidden;
    }


    /**
     * Ajax method
     *
     * @return void
     */
    public function onBoxToggle(): void
    {
        $slug = post('device');
        if (post('visible') === 'false') {
            $visible = false;
        } else {
            $visible = true;
        }

        $sessionHidden = [];
        if (Session::has('session_hidden')) {
            $sessionHidden = Session::get('session_hidden');
        }
        $sessionHidden[$slug] = !$visible;
        Session::put('session_hidden', $sessionHidden);
    }

    /**
     * Ajax method
     * @return void
     */
    public function onUpdateDim(): void
    {
        $this->commandSender->setDim(post('device'), (int)post('value'));
    }

    /**
     * Ajax method
     * @return void
     * @throws \Exception
     */
    public function onSwitchDevice(): void
    {
        $this->commandSender->toggle(post('device'));
        $this->page['slug'] = post('device');
        $deviceDb = $this->repo->getBySlug(post('device'));
        if ($deviceDb) {
            $this->page['device'] = Discovery::getDeviceByName($deviceDb->name);
        }
    }

    /**
     * @param $devices
     * @return array
     */
    private function getDeviceIcons(array $devices): array
    {
        $result = [];
        foreach ($devices as $slug => $device) {
            if (class_basename($device) === 'WemoBulb') {
                $result[$slug] = 'lightbulb';
            } elseif (class_basename($device) === 'WemoSwitch') {
                $result[$slug] = 'plug';
            } else {
                $result[$slug] = 'cogs';
            }
        }

        return $result;
    }

}
