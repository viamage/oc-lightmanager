<?php namespace Viamage\LightManager\Components;

use Cms\Classes\ComponentBase;
use Keios\Apparatus\Classes\JobManager;
use Keios\Apparatus\Models\Job;
use Viamage\LightManager\Jobs\MakeSleep;

/**
 * Class Sleep
 * @package Viamage\LightManager\Components
 */
class Sleep extends ComponentBase
{
    /**
     * @return array
     */
    public function componentDetails(): array
    {
        return [
            'name'        => 'Sleep Component',
            'description' => 'No description provided yet...',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties(): array
    {
        return [
            'slug' => [
                'label'       => 'Slug',
                'description' => 'Device Slug',
                'default'     => '{{ :slug }}',
                'type'        => 'string',
            ],
        ];
    }

    /**
     * @return void
     */
    public function onRun(): void
    {
        $label = 'Sleep '.$this->property('slug');
        if ($job = Job::where('label', $label)->where('status', 1)->first()) {
            $this->page['job'] = $job;
        }
    }

    /**
     * Ajax method
     * @return void
     * @throws \Exception
     */
    public function onSleep(): void
    {
        $device = $this->property('slug');
        $dimDelay = post('delay');
        $job = new MakeSleep($device, $dimDelay);
        /** @var JobManager $jobManager */
        $jobManager = \App::make(JobManager::class);
        $jobManager->dispatch($job, 'Sleep '.$device);
        $this->page['job'] = Job::where('id', $job->jobId)->first();
    }

    /**
     * @return array
     */
    public function onCheckProgress(): array
    {
        $jobId = post('job');
        /** @var Job $job */
        $job = Job::where('id', $jobId)->first();

        return [
            'progress' => (int)round($job->progressPercent(), 0),
        ];
    }

    /**
     * Empty ajax method for partial refresh only
     * @return void
     */
    public function onComplete(): void
    {

    }

    /**
     * Ajax method
     * @return void
     */
    public function onCancelJob(): void
    {
        $id = post('job');
        /** @var JobManager $jobManager */
        $jobManager = \App::make(JobManager::class);
        $jobManager->cancelJob($id);
        /*
        Job::where('id', $id)->update(
            [
                'is_canceled' => true,
            ]
        );
        */
    }
}
