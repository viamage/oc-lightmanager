<?php namespace Viamage\LightManager\Components;

use Cms\Classes\ComponentBase;
use Viamage\LightManager\Models\Task;
use Viamage\LightManager\Repositories\TaskRepository;

/**
 * Class AdvancedSettings
 * @package Viamage\LightManager\Components
 */
class TaskList extends ComponentBase
{
    /**
     * @var TaskRepository
     */
    private $taskRepository;

    /**
     * TaskList constructor.
     * @param null  $cmsObject
     * @param array $properties
     */
    public function __construct($cmsObject = null, array $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $this->taskRepository = \App::make(TaskRepository::class);
    }

    /**
     * @return array
     */
    public function componentDetails(): array
    {
        return [
            'name'        => 'AdvancedSettings Component',
            'description' => 'No description provided yet...',
        ];
    }

    /**
     *
     */
    public function onRun(): void
    {
        $this->page['tasks'] = $this->taskRepository->getAll();
    }

    /**
     * Ajax method
     *
     * @return mixed
     */
    public function onDeleteTask()
    {
        $taskId = post('task');
        Task::where('id', $taskId)->delete();
        Task::cacheClear($taskId);
        return \Redirect::to('/tasks');
    }
}
