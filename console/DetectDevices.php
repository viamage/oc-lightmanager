<?php
/**
 * Created by PhpStorm.
 * User: Łukasz Biały
 * URL: http://keios.eu
 * Date: 8/13/15
 * Time: 2:17 AM
 */

namespace Viamage\LightManager\Console;

use Illuminate\Console\Command;
use Viamage\LightManager\Classes\DeviceSynchronizer;

/**
 * Class Optimize
 * @package Keios\Apparatus\Console
 */
class DetectDevices extends Command
{
    /**
     * The console command name.
     */
    protected $name = 'wemo:detect';

    /**
     * The console command description.
     */
    protected $description = 'Detects devices';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $synchronizer = new DeviceSynchronizer();
        $devices = $synchronizer->scanForDevices();
        $this->info('Detected devices:');
        foreach ($devices as $device) {
            $this->comment($device->name);
        }
    }
}