<?php
/**
 * Created by PhpStorm.
 * User: Łukasz Biały
 * URL: http://keios.eu
 * Date: 8/13/15
 * Time: 2:17 AM
 */

namespace Viamage\LightManager\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Keios\Apparatus\Classes\JobManager;
use Viamage\LightManager\Classes\CommandSender;
use Viamage\LightManager\Jobs\MakeSleep;
use Viamage\LightManager\Models\Task;
use Viamage\LightManager\Repositories\TaskRepository;

/**
 * Class Optimize
 * @package Keios\Apparatus\Console
 */
class RunTasks extends Command
{
    /**
     * The console command name.
     */
    protected $name = 'wemo:run-tasks';

    /**
     * The console command description.
     */
    protected $description = 'Runs tasks';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $repo = new TaskRepository();
        $now = Carbon::now()->format('H:i');
        $day = Carbon::now()->format('l');
        $tasks = $repo->getActive();
        $count = 0;

        /** @var Task $task */
        foreach ($tasks as $task) {
            $actionTime = new Carbon($task->action_time);
            $days = $task->actionDaysToArray();
            $fullDays = array_filter(
                $this->dayMap(),
                function ($e) use ($days) {
                    return \in_array($this->getDayKey($e), $days, true);
                }
            );
            if ($actionTime->format('H:i') === $now && \in_array($day, $fullDays, true)) {
                ++$count;
                $this->info('Running task '.$task->name);
                $this->runTask($task);
            }
        }
        if ($count === 0) {
            $this->info('No tasks to run');
        }
    }

    /**
     * @param $day
     * @return string
     */
    public function getDayKey($day): string
    {
        $map = $this->dayMap();
        if (\in_array($day, $map, true)) {
            return array_flip($map)[$day];
        }

        return '';
    }

    /**
     * @return array
     */
    private function dayMap(): array
    {
        return [
            'mo' => 'Monday',
            'tu' => 'Tuseday',
            'we' => 'Wednesday',
            'th' => 'Thursday',
            'fr' => 'Friday',
            'sa' => 'Saturday',
            'su' => 'Sunday',
        ];
    }

    /**
     * @param Task $task
     * @throws \Exception
     */
    private function runTask(Task $task): void
    {
        switch ($task->action) {
            case 'on':
                $this->runOnTask($task);
                break;
            case 'off':
                $this->runOffTask($task);
                break;
            case 'dim':
                $this->runDimTask($task);
                break;
            default:
                break;
        }
    }

    /**
     * @param Task $task
     */
    private function runOnTask(Task $task): void
    {
        $commandSender = new CommandSender();
        $commandSender->switchOn($task->device->slug);
    }

    /**
     * @param Task $task
     */
    private function runOffTask(Task $task): void
    {
        $commandSender = new CommandSender();
        $commandSender->switchOff($task->device->slug);
    }

    /**
     * @param Task $task
     * @throws \Exception
     */
    private function runDimTask(Task $task): void
    {
        $commandSender = new CommandSender();
        if ($task->dim_time > 1) {
            /** @var JobManager $jobManager */
            $jobManager = \App::make(JobManager::class);
            $job = new MakeSleep($task->device->slug, $task->dim_time, $task->dim);
            $jobManager->dispatch($job, 'Dim '.$task->device->slug);
        } else {
            $commandSender->setDim($task->device->slug, $task->dim);
        }
    }
}