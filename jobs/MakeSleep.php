<?php
/**
 * Copyright (c) 2018 Viamage Limited
 * All Rights Reserved
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of Viamage Limited and its suppliers, if any.
 *  The intellectual and technical concepts contained herein
 *  are proprietary to Viamage Limited and its suppliers and are
 *  protected by trade secret or copyright law, if not specified otherwise.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Viamage Limited.
 *
 */

/**
 * Created by PhpStorm.
 * User: jin
 * Date: 4/15/18
 * Time: 4:43 PM
 */

namespace Viamage\LightManager\Jobs;

use a15lam\PhpWemo\Devices\WemoBulb;
use a15lam\PhpWemo\Discovery;
use App;
use Keios\Apparatus\Classes\JobManager;
use Keios\Apparatus\Contracts\ApparatusQueueJob;
use Viamage\LightManager\Classes\CommandSender;
use Viamage\LightManager\Models\Device;
use Viamage\LightManager\Repositories\DeviceRepository;
use Viamage\LightManager\ValueObjects\SleepValues;

/**
 * Class SendRequestJob
 *
 * Sends POST requests with given data to multiple target urls. Example of Apparatus Job.
 *
 * @package Keios\Apparatus\Jobs
 */
class MakeSleep implements ApparatusQueueJob
{
    /**
     * @var int
     */
    public $jobId;

    /**
     * @var JobManager
     */
    public $jobManager;

    /**
     * @var WemoBulb
     */
    private $device;

    /**
     * @var int
     */
    private $dimTo;

    /**
     * @var int
     */
    private $dimDelay;

    /**
     * @var Device
     */
    private $deviceDb;

    /**
     * @var CommandSender
     */
    private $commandSender;

    /**
     * @param int $id
     */
    public function assignJobId(int $id)
    {
        $this->jobId = $id;
    }

    /**
     * SendRequestJob constructor.
     *
     * We provide array with stuff to send with post and array of urls to which we want to send
     *
     * @param     $slug
     * @param     $dimDelay
     * @param int $dimTo
     * @internal param array $targetUrls
     * @internal param array $data
     * @internal param string $targetUrl
     * @throws \Exception
     */
    public function __construct(string $slug, int $dimDelay, int $dimTo = 0)
    {
        /** @var DeviceRepository $repo */
        $repo = \App::make(DeviceRepository::class);
        $this->deviceDb = $repo->getBySlug($slug);
        $this->device = Discovery::getDeviceByName($this->deviceDb->name);
        $this->dimDelay = $dimDelay;
        $this->dimTo = $dimTo;
        $this->commandSender = new CommandSender($repo);
    }

    /**
     * Job handler. This will be done in background.
     *
     * @param JobManager $jobManager
     * @throws \Exception
     */
    public function handle(JobManager $jobManager): void
    {
        if (!$this->device->isDimmable()) {
            $seconds = $this->dimDelay;
            $jobManager->startJob($this->jobId, $seconds);
            for ($i = 0; $i <= $seconds; ++$i) {
                $jobManager->updateJobState($this->jobId, $i);
                sleep(1);
            }
            $this->commandSender->switchOff($this->deviceDb->slug);
            $jobManager->completeJob($this->jobId, []);

            return;
        }
        $currentState = $this->device->dimState();
        $vars = new SleepValues($currentState, $this->dimTo, $this->dimDelay);
        $jobManager->startJob($this->jobId, $vars->loop);
        for ($i = 0; $i <= $vars->loop; ++$i) {
            if ($jobManager->checkIfCanceled($this->jobId)) {
                $jobManager->cancelJob($this->jobId);

                return;
            }

            dump($vars);
            $this->rediscoverDevice();
            $vars->setCurrentDim($this->device->dimState());
            $vars->current = $i;

            if ($this->validateDecreasingDims($vars)) {
                $jobManager->completeJob($this->jobId, []);

                return;
            }
            if ($this->validateIncreasingDims($vars)) {
                $this->device->dim($vars->targetDim);
                $jobManager->completeJob($this->jobId, []);

                return;
            }

            $this->device->dim($vars->nextDim);
            $jobManager->updateJobState($this->jobId, $i);
            dump('Sleeping...');
            sleep($vars->sleepValue);
        }
        dump('Completing...');
        $jobManager->completeJob($this->jobId, []);
    }

    /**
     * @param SleepValues $vars
     * @return bool
     */
    private function validateIncreasingDims(SleepValues $vars): bool
    {
        if ($vars->dimDifference < 0 && $vars->nextDim >= $vars->targetDim) {
            dump('Next dim would be higher than target. Setting target and completing.');

            return true;
        }

        return false;
    }

    /**
     * @param SleepValues $vars
     * @return bool
     */
    private function validateDecreasingDims(SleepValues $vars): bool
    {
        if ($vars->dimDifference > 0 && $vars->nextDim <= $vars->targetDim) {
            if ($vars->targetDim === 0) {
                dump('Next dim level would be below 0. Switching device off');
                $this->commandSender->switchOff($this->deviceDb->slug);

                return true;
            }

            dump('Next dim level would be over target. Completing now.');

            return true;
        }

        return false;
    }

    /**
     *
     * @throws \Exception
     */
    public function rediscoverDevice(): void
    {
        $this->device = Discovery::getDeviceByName($this->deviceDb->name);
    }
}