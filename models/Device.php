<?php namespace Viamage\LightManager\Models;

use Model;

/**
 * Device Model
 */
class Device extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'viamage_lightmanager_devices';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    protected $jsonable = ['metadata'];
    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'slug',
        'type',
        'ip',
        'metadata',
        'is_online',
    ];

    /**
     * @var array Relations
     */
    public $belongsToMany = [
        'tasks' => [
            Task::class,
            'table'    => 'viamage_lightmanager_tasks_devices',
            'key'      => 'device_id',
            'otherKey' => 'task_id',
        ],
    ];

    public function afterSave(): void
    {
        self::clearCache($this->slug, $this->id);
    }

    public function afterDelete(): void
    {
        self::clearCache($this->slug, $this->id);
    }

    public static function clearCache(string $slug = null, int $id = null): void
    {
        \Cache::forget('device_'.$slug);
        \Cache::forget('device_'.$id);
        \Cache::forget('wemo_devices');
    }
}
