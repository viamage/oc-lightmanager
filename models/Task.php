<?php namespace Viamage\LightManager\Models;

use Carbon\Carbon;
use Model;

/**
 * Task Model
 */
class Task extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'viamage_lightmanager_tasks';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'device' => Device::class,
    ];

    /**
     *
     */
    public function afterSave(): void
    {
        self::cacheClear($this->id);
    }

    /**
     *
     */
    public function afterDelete(): void
    {
        self::cacheClear($this->id);
    }

    /**
     * @param int $id
     */
    public static function cacheClear(int $id): void
    {
        \Cache::forget('task_'.$id);
        \Cache::forget('wemo_tasks');
        \Cache::forget('wemo_active_tasks');
    }

    /**
     * @return string
     */
    public function getActionString(): string
    {
        switch ($this->action) {
            case 'on':
                return 'Switch On';
            case 'off':
                return 'Switch Off';
            case 'dim';
                return 'Dim to '.$this->dim;
            default:
                return 'Unknown';
        }
    }

    /**
     * @return string
     */
    public function getActionTime(): string
    {
        $time = new Carbon($this->action_time);

        return $time->format('H:i');
    }

    /**
     * @return string
     */
    public function getActionDays(): string
    {
        $map = $this->actionDaysToArray();
        $result = '';
        foreach ($map as $day) {
            $result .= '<div class="ui fluid label">'.$day.'</div><br />';
        }

        //return implode(', ', $map);
        return $result;
    }

    /**
     * @return array
     */
    public function actionDaysToArray(): array
    {
        $days = $this->action_days;
        $map = [
            0 => 'mo',
            1 => 'tu',
            2 => 'we',
            3 => 'th',
            4 => 'fr',
            5 => 'sa',
            6 => 'su',
        ];
        $remove = 7 - \strlen($days);
        /** @noinspection ForeachInvariantsInspection */
        for ($i = 0; $i < $remove; ++$i) {
            unset($map[$i]);
        }
        $map = array_flatten($map);
        foreach (str_split($days) as $key => $digit) {
            if ((int)$digit === 0) {
                unset($map[$key]);
            }
        }

        return $map;
    }

    /**
     * @return string
     */
    public function getDeviceLabel(): string
    {
        if ($this->device) {
            return $this->device->name;
        }

        return '';
    }

    /**
     * @return string
     */
    public function getActiveStatus(): string
    {
        if ($this->is_active) {
            return 'Active';
        }

        return 'Not Active';
    }

    public function setActionDaysFromPost(array $weekdays): void
    {
        $map = [
            'mo',
            'tu',
            'we',
            'th',
            'fr',
            'sa',
            'su',
        ];
        $result = '';
        foreach ($map as $day) {
            if (array_key_exists($day, $weekdays) && $weekdays[$day] === 'on') {
                $result .= 1;
            } else {
                $result .= 0;
            }
        }

        $this->action_days = (int)$result;
    }
}
