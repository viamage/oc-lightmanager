<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 5/26/18
 * Time: 10:10 AM
 */

namespace Viamage\LightManager\Repositories;

use October\Rain\Database\Collection;
use Viamage\LightManager\Models\Device;

/**
 * Class DeviceRepository
 * @package Viamage\LightManager\Repositories
 */
class DeviceRepository
{
    /**
     *
     */
    public const table = 'devices';

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return Device::rememberForever('wemo_devices')->get();
    }

    /**
     * @param int $id
     * @return Device
     */
    public function getByid(int $id): ?Device
    {
        return Device::where('id', $id)->first();
    }

    /**
     * @param string $slug
     * @return null|Device
     */
    public function getBySlug(string $slug): ?Device
    {
        return Device::where('slug', $slug)->rememberForever('device_'.$slug)->first();
    }
}