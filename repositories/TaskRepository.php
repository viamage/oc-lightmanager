<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 5/26/18
 * Time: 5:31 PM
 */

namespace Viamage\LightManager\Repositories;

use October\Rain\Database\Collection;
use Viamage\LightManager\Models\Task;

/**
 * Class TaskRepository
 * @package Viamage\LightManager\Repositories
 */
class TaskRepository
{
    /**
     * @return Collection|Task[]
     */
    public function getAll(): Collection
    {
        return Task::rememberForever('wemo_tasks')->get();
    }

    /**
     * @param int $id
     * @return Task|null
     */
    public function getById(int $id): ?Task
    {
        return Task::where('id', $id)->rememberForever('task_'.$id)->first();
    }

    /**
     * @return Collection|Task[]
     */
    public function getActive(): Collection
    {
        return Task::where('is_active', true)->rememberForever('wemo_active_tasks')->get();
    }
}