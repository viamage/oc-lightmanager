<?php namespace Viamage\LightManager\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDevicesTable extends Migration
{
    public function up()
    {
        Schema::create('viamage_lightmanager_devices', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->index();
            $table->string('slug')->index();
            $table->string('type');
            $table->string('ip')->index();
            $table->text('metadata')->nullable();
            $table->boolean('is_online')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('viamage_lightmanager_devices');
    }
}
