<?php namespace Viamage\LightManager\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTasksTable extends Migration
{
    public function up()
    {
        Schema::create('viamage_lightmanager_tasks', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('action');
            $table->integer('device_id')->index();
            $table->integer('dim')->nullable();
            $table->integer('dim_time')->nullable();
            $table->time('action_time');
            $table->integer('action_days');
            $table->boolean('is_active');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('viamage_lightmanager_tasks');
    }
}
