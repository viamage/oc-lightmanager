<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 5/26/18
 * Time: 7:47 PM
 */

namespace Viamage\LightManager\ValueObjects;

/**
 * Class SleepValues
 * @package Viamage\LightManager\ValueObjects
 */
class SleepValues
{
    /**
     * @var int
     */
    public $currentDim;

    /**
     * @var int
     */
    public $targetDim;

    /**
     * @var int
     */
    public $nextDim;

    /**
     * @var int
     */
    public $dimDifference;

    /**
     * @var int
     */
    public $dimmingTime;

    /**
     * @var float|int
     */
    public $dimPerSecond;

    /**
     * @var int
     */
    public $secondsPerDim;

    /**
     * @var int
     */
    public $sleepValue = 1;

    /**
     * @var int
     */
    public $current;

    /**
     * @var int
     */
    public $loop;

    /**
     * SleepValues constructor.
     * @param int $currentDim
     * @param int $targetDim
     * @param int $dimmingTime
     */
    public function __construct(int $currentDim, int $targetDim, int $dimmingTime)
    {
        $this->currentDim = $currentDim;
        $this->targetDim = $targetDim;
        $this->dimmingTime = $dimmingTime;
        $this->loop = $dimmingTime;
        $this->current = 0;
        $this->dimDifference = $currentDim - $targetDim;
        $this->dimPerSecond = $this->dimDifference / $this->dimmingTime;
        $this->nextDim = $this->currentDim - $this->dimPerSecond;

        if ($this->dimPerSecond < 1 && $this->dimPerSecond > 0) {
            $this->secondsPerDim = 1 / $this->dimPerSecond;
            $this->loop = (int)round($this->dimmingTime / $this->secondsPerDim, 0);
            $this->nextDim = $this->currentDim - 1;
        }

        if ($this->dimPerSecond < 0 && $this->dimPerSecond > -1) {
            $this->secondsPerDim = 0 - (1 / $this->dimPerSecond);
            $this->sleepValue = $this->secondsPerDim;
            $this->loop = (int)round($this->dimmingTime / $this->secondsPerDim, 0);
            $this->nextDim = $this->currentDim + 1;
        }
    }

    /**
     * @param int $dim
     */
    public function setCurrentDim(int $dim): void
    {
        $this->currentDim = $dim;
        $this->nextDim = (int)round($this->currentDim - $this->dimPerSecond, 0);
        if ($this->dimPerSecond < 1 && $this->dimPerSecond > 0) {
            $this->nextDim = $this->currentDim - 1;
        }
        if ($this->dimPerSecond < 0 && $this->dimPerSecond > -1) {
            $this->nextDim = $this->currentDim + 1;
        }
    }
}